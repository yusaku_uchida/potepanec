require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:root) { create :taxon }
  let(:child) { create :taxon, parent: root }
  let(:product) { create :product, taxons: [child] }
  let!(:related_product) { create :product, taxons: [child] }

  scenario "check products show" do
    visit potepan_product_path(product.id)
    expect(page).to have_content "一覧ページへ戻る"
    expect(page).to have_content product.name
    expect(page).to have_content product.price
    expect(page).to have_content product.description
    expect(page).to have_content related_product.name
  end

  scenario "click 一覧ページへ戻る" do
    visit potepan_product_path(product.id)
    click_on "一覧ページへ戻る"
    expect(page).to have_content root.name
    expect(page).to have_current_path(potepan_category_path(root.id))
  end

  scenario "click related_product" do
    visit potepan_product_path(product.id)
    click_on related_product.name
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.price
    expect(page).to have_content related_product.description
    expect(page).to have_content product.name
    expect(page).to have_current_path(potepan_product_path(related_product.id))
  end
end
