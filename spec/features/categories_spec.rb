require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create :taxonomy }
  let(:root) { taxonomy.root }
  let!(:child1) { create :taxon, taxonomy: taxonomy, parent: root, name: "Child1" }
  let!(:child2) { create :taxon, taxonomy: taxonomy, parent: root, name: "Child2" }
  let!(:product1) { create :product, taxons: [child1] }
  let!(:product2) { create :product, taxons: [child2] }

  scenario "check categories pages (taxon=root)" do
    visit potepan_category_path(root.id)
    expect(page).to have_content root.name
    expect(page).to have_content taxonomy.name
    expect(page).to have_content child1.name
    expect(page).to have_content child2.name
    expect(page).to have_content product1.name
    expect(page).to have_content product2.name
  end

  scenario "click sidebar's link" do
    visit potepan_category_path(root.id)
    click_on "#{child1.name}"
    expect(page).to have_content child1.name
    expect(page).to have_content product1.name
    expect(page).not_to have_content product2.name
    expect(page).to have_current_path(potepan_category_path(child1.id))
  end
end
