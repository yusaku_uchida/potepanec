require 'rails_helper'

RSpec.feature "Index", type: :feature do
  let!(:product1) { create :product, available_on: 1.month.ago, taxons: [taxon] }
  let!(:product2) { create :product, available_on: 6.month.ago, taxons: [taxon] }
  let(:taxon) { create :taxon }

  scenario "check index" do
    visit potepan_root_path
    expect(page).to have_content "新着商品"
    expect(page).to have_content product1.name
    expect(page).to have_content product1.price
    expect(page).not_to have_content product2.name
  end

  scenario "click new_product" do
    visit potepan_root_path
    click_on product1.name
    expect(page).to have_content product1.name
    expect(page).to have_content product1.price
    expect(page).to have_content product1.description
    expect(page).to have_current_path(potepan_product_path(product1.id))
  end
end
