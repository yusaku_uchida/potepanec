require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe 'Get #index' do
    let(:product1) { create :product, available_on: 1.month.ago }
    let(:product2) { create :product, available_on: 2.month.ago }

    before do
      get :index
    end

    it 'responseがsuccessであること' do
      expect(response).to be_success
    end
    it 'responseが200 OKであること' do
      expect(response.status).to eq 200
    end
    it 'new_productsに新着商品を返すこと' do
      expect(assigns(:new_products)).to match_array [product1, product2]
    end
  end
end
