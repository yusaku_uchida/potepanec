require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'Get #show' do
    let(:product) { create :product, taxons: [taxon1, taxon2] }
    let(:taxon1) { create :taxon }
    let(:taxon2) { create :taxon }
    let!(:related_products1) { create :product, taxons: [taxon1] }
    let!(:related_products2) { create :product, taxons: [taxon2] }

    context '存在するproductを渡した場合' do
      before do
        get :show, params: { id: product.id }
      end

      it 'responseが200 OKであること' do
        expect(response.status).to eq 200
      end
      it '渡したproductを返すこと' do
        expect(assigns(:product)).to eq product
      end
      it '渡したproductの関連商品を返すこと' do
        expect(assigns(:related_products)).to match_array [related_products1, related_products2]
      end
      it 'showテンプレートを返すこと' do
        expect(response).to render_template :show
      end
    end

    context '存在しないproductを渡した場合' do
      it 'RecordNotFoundを返すこと' do
        expect { get 'show', params: { id: 'a' } }.to raise_exception(ActiveRecord::RecordNotFound)
      end
    end
  end
end
