require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "Get #show" do
    let(:root) { create :taxon }
    let!(:child) { create :taxon, parent_id: root.id, products: [product1, product2] }
    let(:product1) { create :product }
    let(:product2) { create :product }

    context 'taxonがrootの場合' do
      before do
        get :show, params: { id: root.id }
      end

      it 'responseがsuccessであること' do
        expect(response).to be_success
      end
      it 'responseが200 OKであること' do
        expect(response.status).to eq 200
      end
      it 'rootを返すこと' do
        expect(assigns(:taxon)).to eq root
      end
      it 'child配下のproductsを返すこと' do
        expect(assigns(:displays)).to match_array child.products
      end
      it 'showテンプレートを返すこと' do
        expect(response).to render_template :show
      end
    end

    context 'taxonがchildの場合' do
      before do
        get :show, params: { id: child.id }
      end

      it 'responseがsuccessであること' do
        expect(response).to be_success
      end
      it 'responseが200 OKであること' do
        expect(response.status).to eq 200
      end
      it 'childを返すこと' do
        expect(assigns(:taxon)).to eq child
      end
      it 'child配下のproductsを返すこと' do
        expect(assigns(:displays)).to match_array child.products
      end
      it 'showテンプレートを返すこと' do
        expect(response).to render_template :show
      end
    end

    context '存在しないtaxonを渡した場合' do
      it 'RecordNotFoundを返すこと' do
        expect { get 'show', params: { id: 'a' } }.to raise_exception(ActiveRecord::RecordNotFound)
      end
    end
  end
end
