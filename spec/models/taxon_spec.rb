require 'rails_helper'

RSpec.describe Spree::Taxon, type: :model do
  let!(:taxon1) { create :taxon, name: "sample1" }
  let!(:taxon2) { create :taxon, name: "sample2" }
  let!(:taxon3) { create :taxon, name: "sample3" }

  describe "scope" do
    describe "search_by_names" do
      it '引数の配列と同じ順番で対象となるtaxonsを返すこと' do
        sample_array = ["sample2", "sample3", "sample1"]
        expect(Spree::Taxon.search_by_names(sample_array)).
          to eq [taxon2, taxon3, taxon1]
      end
    end
  end
end
