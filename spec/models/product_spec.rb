require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let!(:product1) { create :product, taxons: [taxon], available_on: 1.month.ago }
  let!(:product2) { create :product, taxons: [taxon], available_on: 2.month.ago }
  let!(:product3) { create :product, taxons: [taxon], available_on: 3.month.ago }
  let!(:product4) { create :product, taxons: [taxon], available_on: 4.month.ago }
  let!(:product5) { create :product, taxons: [taxon], available_on: 5.month.ago }
  let!(:product00) { create :product }
  let!(:taxon) { create :taxon }

  describe "scope" do
    describe "search_by_taxons" do
      it '引数となるtaxons_idのproductsを返すこと' do
        expect(Spree::Product.search_by_taxons(taxon.id)).
          to match_array [product1, product2, product3, product4, product5]
      end
    end

    describe "except_product" do
      it '引数となるproductを除外すること' do
        expect(Spree::Product.all.except_product(product1)).
          to match_array [product2, product3, product4, product5, product00]
      end
    end

    describe "search_related" do
      it '関連商品の中にtaxons_idが異なるproductが含まれないこと' do
        limit = 100
        expect(Spree::Product.search_related(product1, limit)).
          not_to include(product00)
      end
      it '関連商品の中に引数となるproductが含まれないこと' do
        limit = 100
        expect(Spree::Product.search_related(product1, limit)).
          not_to include(product1)
      end
      it '関連商品の数は引数のlimitの数となること' do
        limit = 3
        expect(Spree::Product.search_related(product1, limit).count).
          to eq 3
      end
    end

    describe "search_rew" do
      it '新着商品は引数のsinceより新しいものであること' do
        since = 3.month.ago
        limit = 100
        expect(Spree::Product.search_new(since, limit)).
          to match_array [product1, product2]
      end
      it '新着商品の数は引数のlimitの数となること' do
        since = 100.year.ago
        limit = 4
        expect(Spree::Product.search_new(since, limit).count).
          to eq 4
      end
    end
  end
end
