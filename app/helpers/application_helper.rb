module ApplicationHelper
  # ページごとのフルタイトルを返します
  def full_title(page_title)
    base_title = "BIGBAG Store"
    if page_title.empty?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def return_type_and_values(variant)
    variant.option_values.map { |x| [x.option_type.presentation, x.presentation] }
  end

  def render_product_displays(collection, where_from)
    render partial: 'shared/product',
           collection: collection,
           as: "product",
           locals: { where_from: where_from }
  end

  # LIST表示するかを判断（params[:list]が無い場合、GRID表示）
  def display_format_is_list?
    session[:format] == 'list'
  end
end
