Spree::Taxon.class_eval do
  scope :search_by_names, ->(name_array) {
    where(name: name_array).
      sort_by { |x| name_array.index(x.name) }
  }
end
