Spree::Product.class_eval do
  scope :search_by_taxons, ->(target) {
    joins(:taxons).
      where('spree_taxons.id': target).
      distinct.
      including_image_price
  }

  scope :including_image_price, -> { includes(master: [:images, :default_price]) }

  scope :except_product, ->(product) { where.not(id: product.id) }

  scope :search_related, ->(product, limit) {
    search_by_taxons(product.taxons.ids).
      except_product(product).
      limit(limit)
  }

  scope :search_new, ->(since, limit) {
    where('available_on > ?', since).
      limit(limit).
      including_image_price
  }

  scope :search_by_searchbox, ->(word) {
    where('name LIKE ? OR description LIKE ?', "%#{word}%", "%#{word}%").
      distinct.
      including_image_price
  }
end
