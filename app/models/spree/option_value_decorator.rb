Spree::OptionValue.class_eval do
  scope :search_by_variants, ->(variants) {
    joins(:variants).
      where('spree_variants': { id: variants }).
      distinct
  }

  scope :search_by_option_type_and_selfname, ->(option_type, selfname) {
    where(option_type: option_type).
      find_by(name: selfname)
  }
end
