Spree::OptionType.class_eval do
  scope :search_by_taxon, ->(taxon) {
    joins({ option_values: { variants: { product: :taxons } } }).
      where('spree_taxons': { id: taxon }).
      distinct
  }
end
