Spree::Variant.class_eval do
  scope :search_by_option_value, ->(option_value) {
    joins(:option_values).
      where('spree_option_values': { id: option_value.id }).
      including_image_price.
      including_option_type
  }

  scope :including_image_price, -> { includes(:images, :default_price) }
  scope :including_option_type, -> { includes(option_values: :option_type) }
end
