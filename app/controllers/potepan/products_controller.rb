class Potepan::ProductsController < ApplicationController
  LIMIT_OF_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.search_related(@product, LIMIT_OF_RELATED_PRODUCTS)
  end
end
