class Potepan::CategoriesController < ApplicationController
  DROPDOWN_LIST = [
                    ["並び替え", ""],
                    ["新着順", "new"],
                    ["古い順", "old"],
                    ["安い順", "cheap"],
                    ["高い順", "expensive"],
                  ].freeze
  def show
    @taxonomies = Spree::Taxonomy.all
    @taxon = Spree::Taxon.find(params[:id])
    @dropdown_list = make_second_elements_url(DROPDOWN_LIST)
    @params = permitted_params

    format_switching
    show_or_not_filter_function
    decide_displays_instance_variable
    word_search_function
    sort_displays_by(params[:select_sort])
  end

  private

  def permitted_params
    params.permit(:id, :option_type, :option_value, :search, :select_sort, :format_switch)
  end

  # 表示フォーマットのスイッチング
  def format_switching
    if params[:format_switch] == 'list'
      session[:format] = 'list'
    elsif params[:format_switch] == 'grid'
      session[:format] = 'grid'
    end
  end

  # option系のparamsを取得
  def get_option_params
    @type = Spree::OptionType.find_by(name: params[:option_type])
    @value = Spree::OptionValue.search_by_option_type_and_selfname(@type, params[:option_value])
  end

  # 条件を満たせばFILTER機能を表示させる
  def show_or_not_filter_function
    if @taxon.leaf? && @taxon.root.name == "Categories"
      @option_types = Spree::OptionType.search_by_taxon(@taxon)
    else
      @option_types = []
    end
  end

  # ドロップダウンリスト選択後のURLクエリパラメーター作成
  def make_url_by_dropdown(order)
    potepan_category_path(params[:id], option_type: params[:option_type],
                                       option_value: params[:option_value],
                                       search: params[:search],
                                       select_sort: order)
  end

  def make_second_elements_url(list)
    list.map { |x| [x[0], make_url_by_dropdown(x[1])] }
  end

  def sort_displays_by(order)
    case order
    when "new"
      @displays = @displays.sort_by { |x| x.available_on }.reverse
    when "old"
      @displays = @displays.sort_by { |x| x.available_on }
    when "cheap"
      @displays = @displays.sort_by { |x| x.price }
    when "expensive"
      @displays = @displays.sort_by { |x| x.price }.reverse
    else
      @displays = @displays.sort_by { |x| x.available_on }.reverse
    end
  end

  def word_search_function
    if searched_words?
      if params[:search] == ""
        @displays = []
      else
        search_word = escape_wildcard(params[:search])
        @displays = Spree::Product.search_by_searchbox(search_word)
      end
    end
  end

  # ワイルドカードを置換してエスケープする
  def escape_wildcard(word)
    word.gsub(/[_%\[]/, '_' => '[_]', '%' => '[%]', '[' => '[[]')
  end

  # @displaysを条件によって変える
  def decide_displays_instance_variable
    if products_was_filtered?
      get_option_params
      @displays = Spree::Variant.search_by_option_value(@value)
    else
      @displays = Spree::Product.search_by_taxons(@taxon.self_and_descendants)
    end
  end
end
