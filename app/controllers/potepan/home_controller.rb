class Potepan::HomeController < ApplicationController
  BORDERLINE_OF_NEW = 3.month.ago # 暫定的に3ヶ月以内より最近を新着商品としています
  LIMIT_OF_NEW_PRODUCTS_DISPLAY = 8 # 暫定的に表示される新着商品を8つまでとしています
  POPULAR_CATEGORIES_NAMES = ["T-Shirts", "Bags", "Mugs"].freeze # 便宜上、T-ShirtsとMugsに変更

  def index
    @new_products = Spree::Product.search_new(BORDERLINE_OF_NEW, LIMIT_OF_NEW_PRODUCTS_DISPLAY)
    @popular_categories = Spree::Taxon.search_by_names(POPULAR_CATEGORIES_NAMES)
  end
end
