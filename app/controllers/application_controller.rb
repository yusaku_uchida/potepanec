class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :products_was_filtered?, :searched_words?

  private

  # filter機能をしようしたかを判断
  def products_was_filtered?
    params.include?("option_type")
  end

  # 検索機能を使用したかを判断
  def searched_words?
    params.include?("search")
  end
end
